#ifndef CREATETEXT_H
#define CREATETEXT_H

#include <QObject>
#include <QtQml>
#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QPainter>
#include <QTextItem>

class CreateText : public QQuickPaintedItem
{
Q_OBJECT
    Q_PROPERTY(QString m_text READ m_text WRITE setM_text NOTIFY m_textChanged)
    Q_PROPERTY(int size READ size WRITE setSize NOTIFY sizeChanged)

public:
    CreateText(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);
    bool eventFilter(QObject *obj, QEvent *event);

    QString m_text() const;
    void setM_text(QString);

    int size() const;
    void setSize(int);

signals:
    void m_textChanged(QString text);
    void sizeChanged(int size);

public:
    QString text;
    QString m_text1;
    int m_size;
    int a=-1;
};
#endif // CREATETEXT_H
