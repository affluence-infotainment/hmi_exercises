import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls 2.12
import MyText 1.0

ApplicationWindow {
    id:w
    visible: true
    width: 640
    height: 480
    title: qsTr("List View Simple Example")

    Component {
        id: del
        Rectangle {
            property string name
            width: parent.width
            height: 25
            color: "lightgray"
            Text {
                anchors.centerIn: parent
                id: nameId
                text: model.name
                color: "blue"
            }
        }
    }
    Component {
        id: del1
        MyText{
            id:myText
            width:180
            height:30
            m_text: model.name
            size:index
        }
    }

    ScrollView {
        id: lv
        width: parent.width/3
        height: parent.height/3*2
        anchors.centerIn: parent

        ListView {
            model: modcpp
            delegate:del1
            spacing: 4
        }
    }

}
