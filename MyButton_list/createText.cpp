#include "createText.h"

CreateText::CreateText(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    setAcceptedMouseButtons(Qt::AllButtons);
    this->installEventFilter(this);
}

void CreateText::paint(QPainter *painter)
{
    qDebug()<<"paint"<<++this->a;
    // reference for scaling
    QRectF bounding_rect = boundingRect();
    QString scaled = this->text;
    QRectF *boundingRect = nullptr;

    // draw a rectangle
    painter->fillRect(bounding_rect,(Qt::lightGray));

    // draw a text
    painter->setPen(QPen(Qt::blue));
    painter->setFont(QFont("Times", 15));
    painter->drawText(bounding_rect,Qt::AlignCenter,scaled,boundingRect);
}
QString CreateText::m_text() const
{
    return m_text1;
}

void CreateText::setM_text(QString text)
{
    m_text1 = text;
    this->text=text;
    update();
    emit m_textChanged(m_text1);
}
int CreateText::size() const
{
    return m_size;
}

void CreateText::setSize(int size)
{
    m_size = size;
    emit sizeChanged(m_size);
}

bool CreateText::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        qDebug()<<"QEvent::MouseButtonPressed, index:"<<m_size;
        return true;
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        qDebug()<<"QEvent::MouseButtonReleased";
        return true;
    } else
    {
        return QObject::eventFilter(obj, event);
    }
}
