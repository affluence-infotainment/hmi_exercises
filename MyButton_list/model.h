#ifndef MODEL_H
#define MODEL_H

#include <QAbstractListModel>
#include <QVector>
#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QPainter>

class Model : public QAbstractListModel
{
    Q_OBJECT

    struct ModelItem {
        QString sName;
        };
    enum {name=Qt::UserRole+1};

public:
    explicit Model(QObject *parent = 0);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

signals:

public slots:

private:
    QVector<ModelItem> vtr;

private:
    void CreateDefaultModel(void);
};

#endif // MODEL_H
