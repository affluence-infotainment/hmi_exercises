import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    Rectangle{
        anchors.fill: parent
        MouseArea{
            anchors.fill: parent
            onClicked: {
                sc.create_rect_object();
            }
        }
    }
}
