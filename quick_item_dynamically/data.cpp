#include "data.h"
#include <QQuickItem>
#include <QRandomGenerator>
#include <QQuickWindow>
#include <QtMath>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
QObject *object;
scene::scene(QQmlApplicationEngine &engine, QObject *parent) : engine(engine),QObject(parent)
{

}

void scene::create_rect_object()
{
//    delete object;
    QQmlComponent component(&engine, QUrl("qrc:/Myrect.qml"));

    object = component.create();
    QQuickItem *item = qobject_cast<QQuickItem*>(object);

    // Set the parent of our created qml rect
    item->setParentItem((QQuickItem*)((QQuickWindow *) engine.rootObjects()[0])->contentItem());

       //Set some random position and color
    item->setProperty("color", QColor::fromRgb(QRandomGenerator::global()->generate()));
//    item->setX(20+qFloor(QRandomGenerator::global()->generateDouble()*200));
//    item->setY(20+qFloor(QRandomGenerator::global()->generateDouble()*200));

}

