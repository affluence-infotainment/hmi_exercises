import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.0
import QtQuick.VirtualKeyboard 2.1
import QtQuick.VirtualKeyboard.Settings 2.1 as Lang
import QtQuick.VirtualKeyboard.Styles 2.1 as Style

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("Keyboard")
    property int screenHeight: height;
    TextField {
        id: control
        width: parent.width
        font.pixelSize: 30
        placeholderText: "One line field"
        focus: Qt.inputMethod.visible;
    }
    MouseArea{
        y:50;width:800;height: 100
        onClicked:{
            console.log(Lang.VirtualKeyboardSettings.locale)
            Lang.VirtualKeyboardSettings.locale="hi_IN"
            Style.KeyboardStyle.keyboardDesignHeight=10
            //            console.log(Lang.VirtualKeyboardSettings.availableLocales)
        }
    }

    InputPanel {
        id: keyboard;
        y: screenHeight; // position the top of the keyboard to the bottom of the screen/display

        anchors.left: parent.left;
        anchors.right: parent.right;
        states: State {
            name: "visible";
            when: keyboard.active;
            PropertyChanges {
                target: keyboard;
                // position the top of the keyboard to the bottom of the text input field
                y: screenHeight/2;
            }
        }
        transitions: Transition {
            from: ""; // default initial state
            to: "visible";
            reversible: true; // toggle visibility with reversible: true;
            ParallelAnimation {
                NumberAnimation {
                    properties: "y";
                    duration: 500;
                    easing.type: Easing.InOutQuad;
                }
            }
        }

    }
}
