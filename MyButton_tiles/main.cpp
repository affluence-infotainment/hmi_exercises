#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "model.h"
#include "createText.h"
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Model mc;
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("modcpp", &mc);

    qmlRegisterType<CreateText>("MyText",1,0,"MyText");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
