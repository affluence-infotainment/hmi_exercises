import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import MyText 1.0

Window {
    id:w
    width: 800; height: 400
    color: "white"
    visible: true

    GridView {
        anchors.fill: parent
        cellWidth: w.width/4; cellHeight: w.height/2
        focus: true
        model: modcpp

        verticalLayoutDirection: Grid.TopToBottom
        layoutDirection: Qt.LeftToRight
        flow: Grid.TopToBottom
        flickableDirection: Flickable.HorizontalFlick

        delegate: Item {
            id:itm
            width: w.width/4; height: w.height/2
            MyText{
                id:myText
                anchors.centerIn: itm
                width: itm.width/2; height: itm.height/2
                m_text: model.name
                size:index
            }
        }

    }
}
