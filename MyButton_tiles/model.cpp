#include "model.h"
#include <QDebug>

Model::Model(QObject *parent) :
    QAbstractListModel(parent)
{
    CreateDefaultModel();
}

void Model::CreateDefaultModel(void)
{
    ModelItem itm;
    for (int a=0; a < 60; ++a) {
        itm.sName = "Affluence "+QString::number(a);
        vtr.push_back(itm);
    }
}

int Model::rowCount(const QModelIndex &parent) const
{
    return vtr.size();
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (i < 0 || i >= vtr.size()) {
        return QVariant();
    }
    if (role == name) {
        return vtr[i].sName;
    }
}

QHash<int, QByteArray> Model::roleNames() const
{
    QHash<int, QByteArray> r =QAbstractListModel::roleNames();
    r[name]="name";
    return r;
}


