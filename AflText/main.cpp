#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include "AflText.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    qmlRegisterType<AflText>("MyStyle", 1, 0, "AflText");

    QQmlApplicationEngine engine;
    engine.load("qrc:/main.qml");

    return app.exec();
}
