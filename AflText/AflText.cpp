#include "AflText.h"
AflText::AflText(QQuickItem *parent) : QQuickPaintedItem(parent)
{
}

void AflText::paint(QPainter *painter)
{
    QRectF bounding_rect = boundingRect();
    QString scaled = m_text1;
    QRectF *boundingRect = nullptr;

    //Draw a text
    painter->setPen(QPen(m_color1));
    if(m_bold1)
        painter->setFont(QFont("Times", m_font1, QFont::Bold));
    else
        painter->setFont(QFont("Times", m_font1));
    painter->drawText(bounding_rect,Qt::AlignCenter,scaled,boundingRect);
}
//text Property
QString AflText::m_text() const
{
    return m_text1;
}

void AflText::setM_text(QString text)
{
    m_text1 = text;
    update();
}

//font size property
int AflText::m_font() const
{
    return m_font1;
}

void AflText::setM_font(int font)
{
    m_font1 = font;
    update();
}

//font color property
QString AflText::m_color() const
{
    return m_text1;
}

void AflText::setM_color(QString color)
{
    m_color1 = color;
    update();
}

//font bold property
bool AflText::m_bold() const
{
    return m_bold1;
}

void AflText::setM_bold(bool bold)
{
    m_bold1 = bold;
    update();
}
