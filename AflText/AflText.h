#ifndef AFLTEXT_H
#define AFLTEXT_H

#include <QObject>
#include <QtQml>
#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QPainter>
#include <QTextItem>

class AflText : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString m_text READ m_text WRITE setM_text)
    Q_PROPERTY(int m_font READ m_font WRITE setM_font)
    Q_PROPERTY(QString m_color READ m_color WRITE setM_color)
    Q_PROPERTY(bool m_bold READ m_bold WRITE setM_bold)

public:
    AflText(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);

    QString m_text() const;
    void setM_text(QString);

    int m_font() const;
    void setM_font(int);

    QString m_color() const;
    void setM_color(QString);

    bool m_bold() const;
    void setM_bold(bool);

signals:

private:
    QString text;
    QString m_text1;
    int m_font1;
    QString m_color1;
    bool m_bold1;

};
#endif // AFLTEXT_H
