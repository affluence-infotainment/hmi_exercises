#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include "createText.h" 

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    qmlRegisterType<CreateText>("MyStyle", 1, 0, "MyText");

    QQmlApplicationEngine engine;
    engine.load("qrc:/main.qml");

    return app.exec();
}
