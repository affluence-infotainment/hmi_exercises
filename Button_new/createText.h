#ifndef CREATETEXT_H
#define CREATETEXT_H

#include <QObject>
#include <QtQml>
#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QPainter>
#include <QTextItem>

class CreateText : public QQuickPaintedItem
{
Q_OBJECT

public:
    CreateText(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);
    bool eventFilter(QObject *obj, QEvent *event);

signals:

private:
    QString text;
    QImage current_image;
};
#endif // CREATETEXT_H
