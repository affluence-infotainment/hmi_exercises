import QtGraphicalEffects 1.0
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import MyStyle 1.0

Window {
    width: 500
    height: 500
    visible: true

    MyText{
        id: liveItem
        anchors.centerIn: parent
        height: parent.height/2
        width: parent.width/2
    }
}
