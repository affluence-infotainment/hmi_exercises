#include "createText.h"
CreateText::CreateText(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    setAcceptedMouseButtons(Qt::AllButtons);
    this->installEventFilter(this);
    this->text="Affluence";
    this->current_image = QImage(":/img/blue.jpg");
}

void CreateText::paint(QPainter *painter)
{
    // reference for scaling
    QRectF bounding_rect = boundingRect();
    QString scaled = this->text;
    QImage scaled_img = this->current_image.scaledToHeight(bounding_rect.height());
    QPointF center = bounding_rect.center() - scaled_img.rect().center();
    QRectF *boundingRect = nullptr;

    // draw an image
    painter->drawImage(center, scaled_img);

    // draw a text
    painter->setPen(QPen(Qt::red));
    painter->setFont(QFont("Times", 30, QFont::Bold));
    painter->drawText(bounding_rect,Qt::AlignCenter,scaled,boundingRect);

    // draw a rectangle
    painter->setPen(QPen(Qt::blue));
    painter->drawRect(0,0,bounding_rect.width()-1,bounding_rect.height()-1);
}

bool CreateText::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        qDebug() <<" QEvent::MouseButtonPress ";
        this->text="Gajanan";
        update();
        return true;
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        qDebug() <<" QEvent::MouseButtonRelease ";
        return true;
    } else
    {
        return QObject::eventFilter(obj, event);
    }
}
