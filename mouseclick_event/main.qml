import QtQuick 2.3
import QtQuick.Controls 1.0
import QtQuick.Window 2.12
import Test 1.0

Window{
    visible: true
    width: 500
    height: 500
    id:parentRect
    Rectangle {
        anchors.centerIn: parent
        width: parent.width/5
        height: parent.height/5
        visible: true
        color: "red"
        MyItem {
            anchors.fill: parent
        }
    }
}


