#ifndef MOUSECLICK_H
#define MOUSECLICK_H
#include <QtQuick>

class MyItem : public QQuickItem
{
public:
    MyItem();
    bool eventFilter(QObject *obj, QEvent *event);
};


#endif // MOUSECLICK_H
