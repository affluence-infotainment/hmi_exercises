#include <mouseclick.h>

MyItem::MyItem() {
    setAcceptedMouseButtons(Qt::AllButtons);
    this->installEventFilter(this);
}

bool MyItem::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        qDebug() <<" QEvent::MouseButtonPress ";
        return true;
    }
    else if (event->type() == QEvent::MouseButtonDblClick)
    {
        qDebug() <<" QEvent::MouseButtonDoublelClick ";
        return true;
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        qDebug() <<" QEvent::MouseButtonRelease ";
        return true;
    } else
    {
        return QObject::eventFilter(obj, event);
    }
}
