#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick>
#include <mouseclick.h>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<MyItem>("Test", 1, 0, "MyItem");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
